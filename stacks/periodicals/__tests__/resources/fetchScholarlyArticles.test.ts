jest.mock('../../src/services/scholarlyArticles', () => ({
  fetchNewestIndependentScholarlyArticles: jest.fn().mockReturnValue(Promise.resolve([
    {
      author: [ {
        identifier: 'Arbitrary account id',
      } ],
      identifier: 'arbitrary_identifier',
    },
  ])),
  fetchScholarlyArticlesByAuthor: jest.fn().mockReturnValue(Promise.resolve([
    {
      author: [ {
        identifier: 'Arbitrary account id',
      } ],
      identifier: 'arbitrary_identifier',
    },
  ])),
}));

import { fetchScholarlyArticles } from '../../src/resources/fetchScholarlyArticles';

const mockContext = {
  database: {} as any,

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/articles/'  ],
  path: '/articles/',
  query: null,
};

it('should error when only invalid query params were specified', () => {
  const promise = fetchScholarlyArticles({ ...mockContext, query: { periodical: 'arbitrary data' } });

  return expect(promise).rejects
    .toEqual(new Error('Fetching more than a single author\'s articles is not yet supported.'));
});

describe('fetching recent articles published independently', () => {
  it('should error when articles could not be fetched', () => {
    const mockedArticleService = require.requireMock('../../src/services/scholarlyArticles');
    mockedArticleService.fetchNewestIndependentScholarlyArticles.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    const promise = fetchScholarlyArticles({
      ...mockContext,
      query: {},
    });

    return expect(promise).rejects.toEqual(new Error('Could not load the latest articles, please try again.'));
  });

  it('should call the command handler when all parameters are correct', (done) => {
    const mockedCommandHandler = require.requireMock('../../src/services/scholarlyArticles');

    const promise = fetchScholarlyArticles({
      ...mockContext,
      params: [ '/articles/' ],
      path: '/articles/',
      query: {},
    });

    setImmediate(() => {
      expect(mockedCommandHandler.fetchNewestIndependentScholarlyArticles.mock.calls.length).toBe(1);
      expect(mockedCommandHandler.fetchNewestIndependentScholarlyArticles.mock.calls[0].length).toBe(1);
      done();
    });
  });
});

describe('fetching articles by a single author', () => {
  it('should error when articles could not be fetched', () => {
    const mockedArticleService = require.requireMock('../../src/services/scholarlyArticles');
    mockedArticleService.fetchScholarlyArticlesByAuthor.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    const promise = fetchScholarlyArticles({
      ...mockContext,
      query: { author: 'some_id' },
    });

    return expect(promise).rejects.toEqual(new Error('Could not find articles for author ID `some_id`.'));
  });

  it('should call the command handler when all parameters are correct', (done) => {
    const mockedCommandHandler = require.requireMock('../../src/services/scholarlyArticles');

    const promise = fetchScholarlyArticles({
      ...mockContext,
      params: [ '/articles/' ],
      path: '/articles/',
      query: { author: 'some_id' },
    });

    setImmediate(() => {
      expect(mockedCommandHandler.fetchScholarlyArticlesByAuthor.mock.calls.length).toBe(1);
      expect(mockedCommandHandler.fetchScholarlyArticlesByAuthor.mock.calls[0][1]).toBe('some_id');
      done();
    });
  });
});

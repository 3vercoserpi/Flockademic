jest.mock('react-ga');
jest.mock('../../src/services/periodical', () => ({
  getPeriodical: jest.fn().mockReturnValue(Promise.resolve(
    { identifier: 'arbitrary journal id', creator: { identifier: 'arbitrary user id' } },
  )),
  isPeriodicalSlugAvailable: jest.fn().mockReturnValue(Promise.resolve(true)),
  makePeriodicalPublic: jest.fn().mockReturnValue(Promise.resolve()),
  updatePeriodical: jest.fn().mockReturnValue(Promise.resolve({ result: 'arbitrary periodical' })),
}));
jest.mock('../../src/services/account', () => ({
  getSession: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary user id' })),
}));
jest.mock('../../src/components/orcidButton/component', () => ({
  OrcidButton: 'div',
}));
jest.mock('../../../../lib/utils/periodicals', () => ({
  isPeriodicalOwner: jest.fn().mockReturnValue(true),
  isPublic: jest.fn().mockReturnValue(true),
}));

import {
  BareJournalManagementPage,
  JournalManagementPageProps,
  JournalManagementPageRouteParams,
} from '../../src/components/journalManagementPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';
import { MemoryRouter, Redirect } from 'react-router';

const mockProps: JournalManagementPageProps = {
};
function initialisePage({
  onUpdate = jest.fn(),
  orcid = mockProps.orcid,
  slug = 'arbitrary_slug',
}: Partial<JournalManagementPageProps & JournalManagementPageRouteParams> = {}) {
  return mount(
    <BareJournalManagementPage match={{ params: { slug }, url: 'https://arbitrary_url' }} orcid={orcid} onUpdate={onUpdate}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the Journal details are loading', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display an error message when the journal could not be loaded', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('.message.is-danger')).toExist();
    done();
  }
});

// tslint:disable-next-line:max-line-length
it('should display an error message when the current user does not manage this journal and the journal is public', (done) => {
  const mockedPeriodicalUtils = require.requireMock('../../../../lib/utils/periodicals');
  mockedPeriodicalUtils.isPeriodicalOwner.mockReturnValueOnce(false);
  mockedPeriodicalUtils.isPublic.mockReturnValueOnce(true);

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('.message.is-danger')).toExist();
    done();
  }
});

// tslint:disable-next-line:max-line-length
it('should not display an error message when the current user does not manage this journal but the journal is not public yet', (done) => {
  const mockedPeriodicalUtils = require.requireMock('../../../../lib/utils/periodicals');
  mockedPeriodicalUtils.isPeriodicalOwner.mockReturnValueOnce(true);
  mockedPeriodicalUtils.isPublic.mockReturnValueOnce(false);

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('.message.is-danger')).not.toExist();
    done();
  }
});

it('should display the configuration form when the Journal details are loaded', (done) => {
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(toJson(page.find(BareJournalManagementPage))).toMatchSnapshot();
    done();
  }
});

it('should link the labels to their respective form fields', (done) => {
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();

    const controls = page.find('div.field');
    expect(controls.at(0).find('label').prop('htmlFor')).toBeDefined();
    expect(controls.at(0).find('label').prop('htmlFor')).toBe(controls.at(0).find('input').prop('id'));
    expect(controls.at(1).find('label').prop('htmlFor')).toBeDefined();
    expect(controls.at(1).find('label').prop('htmlFor')).toBe(controls.at(1).find('input').prop('id'));
    expect(controls.at(2).find('label').prop('htmlFor')).toBeDefined();
    expect(controls.at(2).find('label').prop('htmlFor')).toBe(controls.at(2).find('textarea').prop('id'));
    done();
  }
});

it('should render a warning when the chosen name results in a taken URL', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('input[name="name"]').simulate('change', { target: { name: 'name', value: 'Arbitrary name' } });

    page.update();
    mockedPeriodicalService.isPeriodicalSlugAvailable.mockReturnValueOnce(Promise.resolve(false));

    page.find('input[name="name"]').simulate('blur');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(page.find('.help.is-danger')).toExist();
    expect(page.find('.help.is-danger').text()).toMatch('Please try a different name.');
    done();
  }
});

it('should mark "Name your Journal" as done when it\'s done', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.resolve({
    creator: { identifier: 'arbitrary user id' },
    identifier: 'Arbitrary Journal ID',
    name: 'Arbitrary name',
  }));

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    expect(page.find('#nameTodo').hasClass('journalProgress__todo--done')).toBe(true);
    done();
  });
});

it('should submit the form field values when updating the form', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('.update-form [name="description"]').first()
      .simulate('change', { target: { name: 'description', value: 'Some description' } });
    page.find('.update-form [name="headline"]').first()
      .simulate('change', { target: { name: 'headline', value: 'Some headline' } });
    page.find('.update-form [name="name"]').first()
      .simulate('change', { target: { name: 'name', value: 'Some name' } });
    page.find('.update-form').first().simulate('submit');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(mockedPeriodicalService.updatePeriodical.mock.calls.length).toBe(1);
    expect(mockedPeriodicalService.updatePeriodical.mock.calls[0][1]).toEqual({
      description: 'Some description',
      headline: 'Some headline',
      name: 'Some name',
    });
    done();
  }
});

it('should send an analytics event when the user updates the journal details', (done) => {
  const mockedReactGa = require.requireMock('react-ga');
  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('.update-form').first().simulate('submit');
    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Update details',
      category: 'Journal Management',
    });

    done();
  });
});

it('should display a success message when the journal details were successfully saved', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.updatePeriodical.mockReturnValueOnce(Promise.resolve({
    result: {
      name: 'Arbitrary name',
    },
  }));
  const updateSuccessDispatch = jest.fn();

  const page = initialisePage({ onUpdate: updateSuccessDispatch });

  setImmediate(() => {
    page.update();
    page.find('.update-form').first().simulate('submit');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(updateSuccessDispatch.mock.calls.length).toBe(1);
    expect(updateSuccessDispatch.mock.calls[0][0]).toMatchSnapshot();
    done();
  }
});

it('should display an error message when the journal details could not be saved', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  const updatePromise = Promise.reject('Save error');
  updatePromise.catch(() => '');
  mockedPeriodicalService.updatePeriodical.mockReturnValueOnce(updatePromise);

  const page = initialisePage();

  setImmediate(() => {
    page.update();
    page.find('.update-form').first().simulate('submit');
    setImmediate(checkAssertions);
  });

  function checkAssertions() {
    page.update();
    expect(page.find('.is-danger').text())
      .toMatch('There was a problem updating the journal details, please try again.');
    done();
  }
});

it('should display a success message when a header image was successfully uploaded', (done) => {
  const updateSuccessDispatch = jest.fn();

  const page = initialisePage({ onUpdate: updateSuccessDispatch });

  setImmediate(() => {
    page.update();
    const uploadCallback = page.find('HeaderManager').prop('onUpload');
    uploadCallback();

    expect(updateSuccessDispatch.mock.calls.length).toBe(1);
    expect(updateSuccessDispatch.mock.calls[0][0]).toMatchSnapshot();
    done();
  });
});

it('should mark "Sign in / create account" as done when it\'s done', (done) => {
  const page = initialisePage({ orcid: 'Arbitrary ORCID' });

  setImmediate(() => {
    page.update();
    expect(page.find('#authenticationTodo').hasClass('journalProgress__todo--done')).toBe(true);
    done();
  });
});

it('should not allow making the Journal public when no name has been set yet', (done) => {
  const page = initialisePage({ orcid: 'Arbitrary ORCID' });

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();

    expect(page.find('.publication-form button[type="submit"]').prop('disabled')).toBe(true);
    page.find('.publication-form').first().simulate('submit');

    expect(page.state('isBeingMadePublic')).toBe(false);
    done();
  }
});

it('should not allow making the Journal public when the user has not linked their ORCID yet', (done) => {
  const page = initialisePage({ orcid: undefined });

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    page.setState({ periodical: { name: 'Arbitrary name' } });

    expect(page.find('.publication-form button[type="submit"]').prop('disabled')).toBe(true);
    page.find('.publication-form').first().simulate('submit');
    page.update();

    expect(page.state('isBeingMadePublic')).toBe(false);
    done();
  }
});

it('should not display the "Make public" button if the Journal is already public', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.resolve({
    creator: { identifier: 'arbitrary user id' },
    datePublished: '1337-01-01T00:00:00.000Z',
    identifier: 'arbitrary journal id',
  }));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('.publication-form')).not.toExist();
    done();
  }
});

it('should display a spinner in the publish button while the journal is being made public', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.makePeriodicalPublic.mockReturnValueOnce(new Promise(() => null));

  const page = initialisePage({ orcid: 'Arbitrary ORCID' });

  setImmediate(makePublic);
  function makePublic() {
    page.update();
    page.setState({ isAuthorised: true, isBeingMadePublic: false, periodical: { name: 'Arbitrary name' } });

    expect(page.find('.publication-form .is-loading')).not.toExist();
    page.find('.publication-form').first().simulate('submit');

    setImmediate(checkAssertions);
  }

  function checkAssertions() {
    page.update();

    expect(page.find('.publication-form .is-loading')).toExist();
    done();
  }
});

it('should send an analytics event when the user makes the journal public', (done) => {
  const mockedReactGa = require.requireMock('react-ga');
  const page = initialisePage({ orcid: 'Arbitrary ORCID' });

  setImmediate(() => {
    page.update();
    page.setState({ isAuthorised: true, isBeingMadePublic: false, periodical: { name: 'Arbitrary name' } });
    page.find('.publication-form').first().simulate('submit');

    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Make public',
      category: 'Journal Management',
    });

    done();
  });
});

it('should redirect to the journal\'s overview page after it has successfully been made public', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.makePeriodicalPublic.mockReturnValueOnce(Promise.resolve({
    result: { identifier: 'some-slug' },
  }));

  const page = initialisePage({ orcid: 'Arbitrary ORCID' });

  setImmediate(makePublic);
  function makePublic() {
    page.update();
    page.setState({ isBeingMadePublic: false, periodical: { name: 'Arbitrary name' } });

    page.find('.publication-form').first().simulate('submit');

    setImmediate(checkAssertions);
  }

  function checkAssertions() {
    page.update();

    expect(page.find(Redirect)).toExist();
    expect(page.find(Redirect).prop('to')).toBe(('/journal/some-slug'));
    done();
  }
});

// I'm running against a React-DOM error ("Cannot read property 'createEvent' of undefined")
// for this test that I cannot explain. After spending an hour on it, I'm disabling it for now...
it.skip('should hide the spinner when there was an error making the Journal public', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodical.mockReturnValueOnce(Promise.resolve(
      { id: 'arbitrary journal id', creator: { id: 'arbitrary user id' }, name: 'Arbitrary name' },
  ));
  mockedPeriodicalService.makePeriodicalPublic.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const page = initialisePage({ orcid: 'Arbitrary ORCID' });

  setImmediate(makePublic);
  function makePublic() {
    page.update();

    const submitCallback = page.find('form').first().simulate('submit');
    submitCallback({ preventDefault: jest.fn() });
    page.update();

    setImmediate(checkAssertions);
  }

  function checkAssertions() {
    page.update();

    expect(page.find('.publication-form').find(Spinner)).not.toExist();
    done();
  }
});

export interface Account {
  identifier: string;
  orcid?: string;
}

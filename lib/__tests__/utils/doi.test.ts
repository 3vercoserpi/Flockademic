import {
  convertToDoi,
  convertToDoiLink,
  isDoiLink,
} from '../../utils/doi';

describe('convertToDoi', () => {
  it('should properly convert a doi.org link to a DOI', () => {
    expect(convertToDoi('https://doi.org/10.1371/journal.pbio.1002456'))
      .toBe('10.1371/journal.pbio.1002456');
  });

  it('should properly convert a dx.doi.org link to a DOI', () => {
    expect(convertToDoi('https://dx.doi.org/10.1371/journal.pbio.1002456'))
      .toBe('10.1371/journal.pbio.1002456');
  });

  it('should properly convert a doai.io link to a DOI', () => {
    expect(convertToDoi('https://doai.io/10.1371/journal.pbio.1002456'))
      .toBe('10.1371/journal.pbio.1002456');
  });

  it('should properly convert an insecure doi.org link to a DOI', () => {
    expect(convertToDoi('http://doi.org/10.1371/journal.pbio.1002456'))
      .toBe('10.1371/journal.pbio.1002456');
  });

  it('should properly convert an insecure dx.doi.org link to a DOI', () => {
    expect(convertToDoi('http://dx.doi.org/10.1371/journal.pbio.1002456'))
      .toBe('10.1371/journal.pbio.1002456');
  });

  it('should properly convert an insecure doai.io link to a DOI', () => {
    expect(convertToDoi('http://doai.io/10.1371/journal.pbio.1002456'))
      .toBe('10.1371/journal.pbio.1002456');
  });

  it('should return null if the given string is not a DOI link', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(convertToDoi('https://orcid.org/0000-0002-4013-988X/work/1337'))
      .toBeNull();
  });
});

describe('convertToDoiLink', () => {
  it('should properly convert DOIs to DOI links (using doai.io)', () => {
    expect(convertToDoiLink('10.1371/journal.pbio.1002456'))
      .toBe('https://doai.io/10.1371/journal.pbio.1002456');
  });
});

describe('isDoiLink', () => {
  it('should recognise doi.org links', () => {
    expect(isDoiLink('https://doi.org/10.1371/journal.pbio.1002456'))
      .toBe(true);
  });

  it('should recognise dx.doi.org links', () => {
    expect(isDoiLink('https://dx.doi.org/10.1371/journal.pbio.1002456'))
      .toBe(true);
  });

  it('should recognise doai.io links', () => {
    expect(isDoiLink('https://doai.io/10.1371/journal.pbio.1002456'))
      .toBe(true);
  });

  it('should recognise insecure doi.org links', () => {
    expect(isDoiLink('http://doi.org/10.1371/journal.pbio.1002456'))
      .toBe(true);
  });

  it('should recognise insecure dx.doi.org links', () => {
    expect(isDoiLink('http://dx.doi.org/10.1371/journal.pbio.1002456'))
      .toBe(true);
  });

  it('should recognise insecure doai.io links', () => {
    expect(isDoiLink('http://doai.io/10.1371/journal.pbio.1002456'))
      .toBe(true);
  });

  it('should not recognise strings that are not DOI links', () => {
    expect(isDoiLink('10.1371/journal.pbio.1002456'))
      .toBe(false);
  });
});
